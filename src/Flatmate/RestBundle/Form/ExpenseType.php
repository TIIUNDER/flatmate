<?php

namespace Flatmate\RestBundle\Form;

use Flatmate\UtilitiesBundle\Entity\Expense;
use FOS\RestBundle\Form\Transformer\EntityToIdObjectTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExpenseType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category')
            ->add('date', 'date')
            ->add('name')
            ->add('count')
            ->add('consumption')
            ->add('deposit')
            ->add('fee')
            ->add('fee_period_count')
            ->add('fee_period_type')
            ->add('cost_per_unit')
            ->add('period_count')
            ->add('period_type')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Flatmate\UtilitiesBundle\Entity\Expense',
            'csrf_protection' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'expense';
    }
}
