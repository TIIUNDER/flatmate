<?php
namespace Flatmate\RestBundle\Controller;

use Flatmate\RestBundle\Form\ConsumptionType;
use Flatmate\UtilitiesBundle\Entity\Consumption;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Config\Definition\Exception\Exception;

class ConsumptionController extends FOSRestController
{

    /**
     * @return array
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $userId = $this->getUser()->getId();

        if (!$userId) {
            throw $this->createAccessDeniedException($this->get('translator')->trans('exception.access_denied'));
        }

        $entities = $em->getRepository('FlatmateUtilitiesBundle:Consumption')->findByUserId($userId);

        $consumptions = array();
        /** @var Consumption $consumption */
        foreach ($entities as $consumption) {
            // remove other object from API output
            $consumption->setUser(null);
            $consumption->setCategory(null);
            array_push($consumptions, $consumption);
        }

        return array(
            'consumptions' => $consumptions,
        );
    }

    /**
     * @param $id
     * @return array
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Consumption $entity */
        $entity = $em->getRepository('FlatmateUtilitiesBundle:Consumption')->find($id);

        $userId = $this->getUser()->getId();

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.entity_not_found'));
        }

        if ($userId != $entity->getUserId()) {
            throw $this->createAccessDeniedException($this->get('translator')->trans('exception.access_denied'));
        }

        $entity->setUser(null);
        $entity->setCategory(null);

        return array(
            'consumption' => $entity,
        );
    }

    /**
     * @param Request $request
     * @return View|Response
     */
    public function postAction(Request $request)
    {
        $entity = new Consumption();

        $form = $this->createForm(new ConsumptionType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $entity->setUser($this->getUser());
            $entity->setCreatedAt(new \DateTime("now"));

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $response = new Response();
            $response->setStatusCode(200);

            return $response;
        }

        return View::create($form, 400);
    }

    /**
     * @param $id
     * @param Request $request
     * @return View|Response
     */
    public function updateAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FlatmateUtilitiesBundle:Consumption')->find($id);

        $userId = $this->getUser()->getId();

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.entity_not_found'));
        }

        if ($userId != $entity->getUserId()) {
            throw $this->createAccessDeniedException($this->get('translator')->trans('exception.access_denied'));
        }

        $form = $this->createForm(new ConsumptionType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $response = new Response();
            $response->setStatusCode(200);

            return $response;
        }

        return View::create($form, 400);
    }

    /**
     * @param $id
     * @param Request $request
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FlatmateUtilitiesBundle:Consumption')->find($id);

        $userId = $this->getUser()->getId();

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.entity_not_found'));
        }

        if ($userId != $entity->getUserId()) {
            throw $this->createAccessDeniedException($this->get('translator')->trans('exception.access_denied'));
        }

        $response = new Response();

        try {
            $em->remove($entity);
            $em->flush();

            $response->setStatusCode(200);
        } catch (Exception $exception) {
            $response->setStatusCode(400);
        }

        return $response;
    }

}