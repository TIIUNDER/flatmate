<?php
namespace Flatmate\RestBundle\Controller;

use Flatmate\UtilitiesBundle\Entity\Category;
use Flatmate\RestBundle\Form\CategoryType;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends FOSRestController
{
    /**
     * @return array
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $userId = $this->getUser()->getId();

        if (!$userId) {
            throw $this->createAccessDeniedException($this->get('translator')->trans('exception.access_denied'));
        }

        $entities = $em->getRepository('FlatmateUtilitiesBundle:Category')->findByUserId($userId);

        $categories = array();
        /** @var Category $category */
        foreach ($entities as $category) {
            $category->setUser(null); // remove user object from API output
            array_push($categories, $category);
        }

        return array(
            'categories' => $categories,
        );
    }

    /**
     * @param $id
     * @return array
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FlatmateUtilitiesBundle:Category')->find($id);

        $userId = $this->getUser()->getId();

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.entity_not_found'));
        }

        if ($userId != $entity->getUserId()) {
            throw $this->createAccessDeniedException($this->get('translator')->trans('exception.access_denied'));
        }

        $entity->setUser(null);

        return array(
            'category' => $entity,
        );
    }

    /**
     * @param Request $request
     * @return View|Response
     */
    public function postAction(Request $request)
    {
        $entity = new Category();

        $form = $this->createForm(new CategoryType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $entity->setUser($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $response = new Response();
            $response->setStatusCode(200);

            return $response;
        }

        return View::create($form, 400);
    }

    /**
     * @param $id
     * @param Request $request
     * @return View|Response
     */
    public function updateAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FlatmateUtilitiesBundle:Category')->find($id);

        $userId = $this->getUser()->getId();

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.entity_not_found'));
        }

        if ($userId != $entity->getUserId()) {
            throw $this->createAccessDeniedException($this->get('translator')->trans('exception.access_denied'));
        }

        $form = $this->createForm(new CategoryType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $response = new Response();
            $response->setStatusCode(200);

            return $response;
        }

        return View::create($form, 400);
    }

    /**
     * @param $id
     * @param Request $request
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FlatmateUtilitiesBundle:Category')->find($id);

        $userId = $this->getUser()->getId();

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.entity_not_found'));
        }

        if ($userId != $entity->getUserId()) {
            throw $this->createAccessDeniedException($this->get('translator')->trans('exception.access_denied'));
        }

        $response = new Response();

        try {
            $em->remove($entity);
            $em->flush();

            $response->setStatusCode(200);
        } catch (Exception $exception) {
            $response->setStatusCode(400);
        }

        return $response;
    }
}