<?php

namespace Flatmate\RestBundle\Controller;

use Flatmate\RestBundle\Form\UserType;
use Flatmate\UserBundle\Entity\User;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;

class UserController extends FOSRestController
{
    /**
     * @param Request $request
     * @return bool
     */
    public function loginAction(Request $request) {
        // user entity by form
        $entity = new User();

        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new UserType(), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            // user entity by user database
            $user = $em->getRepository('FlatmateUserBundle:User')->findOneByUsername($entity->getUsername());

            if($user) {
                $encoderService = $this->get('security.encoder_factory');
                $encoder = $encoderService->getEncoder($user);
                $encoderPass = $encoder->encodePassword($entity->getPassword(), $user->getSalt());

                if($user->getPassword() == $encoderPass) {
                    return true;
                }
            }
        }

        return false;
    }
}