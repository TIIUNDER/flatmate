<?php
namespace Flatmate\RestBundle\Controller;

use Flatmate\UtilitiesBundle\Helper\ForecastHelper;
use FOS\RestBundle\Controller\FOSRestController;

class ForecastController extends FOSRestController
{
    /**
     * @return array
     */
    public function indexAction() {

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $entities = ForecastHelper::generateForecasts($em, $user);

        $forecasts = array();
        /** @var array $forecast */
        foreach ($entities as $forecast) {
            $forecast['categoryId'] = $forecast['category']->getId();
            $forecast['category'] = null;
            $forecast['name'] = $forecast['expense']->getName();
            $forecast['expense'] = null;
            $forecast['consumption'] = null;
            array_push($forecasts, $forecast);
        }

        return array(
            'forecasts' => $forecasts,
        );
    }

    /**
     * @param $id
     * @return array
     */
    public function showAction($id) {

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $forecasts = ForecastHelper::generateForecasts($em, $user);

        $entity = null;
        foreach($forecasts as $forecast) {
            if($forecast['id'] == $id) {
                $entity = $forecast;
                $entity['categoryId'] = $entity['category']->getId();
                $entity['category'] = null;
                $entity['name'] = $entity['expense']->getName();
                $entity['expense'] = null;
                $entity['consumption'] = null;
                break;
            }
        }

        return array(
            'forecast' => $entity,
        );
    }
}