<?php
/**
 * Created by PhpStorm.
 * User: cneubauer
 * Date: 07.01.15
 * Time: 20:05
 */

// src/Flatmate/UserBundle/DataFixtures/ORM/LoadUserData.php

namespace Flatmate\UserBundle\Datafixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Flatmate\UserBundle\Entity\User;

class LoadUserData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $userSuperAdmin = new User();
        $userSuperAdmin->setUsername("SuperAdmin");
        $userSuperAdmin->setEmail('superAdmin@flatmate.com');
        $userSuperAdmin->setPlainPassword('superAdminPass');
        $userSuperAdmin->setEnabled(true);

        $UserADMIN = new User();
        $UserADMIN->setUsername('Admin');
        $UserADMIN->setEmail('admin@flatmate.com');
        $UserADMIN->setPlainPassword('adminPass');
        $UserADMIN->setEnabled(true);


        $UserAnne = new User();
        $UserAnne->setUsername('Anne');
        $UserAnne->setEmail('anne@flatmate.com');
        $UserAnne->setPlainPassword('annePass');
        $UserAnne->setEnabled(true);


        $UserChristian = new User();
        $UserChristian->setUsername('Christian');
        $UserChristian->setEmail('christian@flatmate.com');
        $UserChristian->setPlainPassword('chrisPass');
        $UserChristian->setEnabled(true);

        $UserFelix = new User();
        $UserFelix->setUsername('Felix');
        $UserFelix->setEmail('felix@flatmate.com');
        $UserFelix->setPlainPassword('felixPass');
        $UserFelix->setEnabled(true);

        $manager->persist($userSuperAdmin);
        $manager->persist($UserADMIN);
        $manager->persist($UserAnne);
        $manager->persist($UserChristian);
        $manager->persist($UserFelix);


        $manager->flush();

    }
    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }

} 