<?php

namespace Flatmate\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('username', null, array('label' => 'user.field.username'))
            ->add('email', null, array('label' => 'user.field.email'))
            ->add('password', 'password', array(
                'required' => false,
                'label' => 'user.field.password',
            ))
            ->add('enabled', 'checkbox', array(
                'required' => false,
                'label' => 'user.field.enabled',
            ))
            ->add('locked', 'checkbox', array(
                'required' => false,
                'label' => 'user.field.locked',
            ))
            ->add('expired', 'checkbox', array(
                'required' => false,
                'label' => 'user.field.expired',
            ))
            ->add('roles', 'collection', array(
                'type' => 'choice',
                'options' => array(
                    'choices' => array(
                        'ROLE_USER' => 'User',
                        'ROLE_ADMIN' => 'Administrator',
                        'ROLE_SUPER_ADMIN' => 'Super Administrator',
                    ),
                ),
                'label' => 'user.field.roles',
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Flatmate\UserBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'flatmate_userbundle_user';
    }
}
