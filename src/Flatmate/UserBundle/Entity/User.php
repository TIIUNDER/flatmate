<?php
/**
 * Created by PhpStorm.
 * User: cneubauer
 * Date: 12.12.14
 * Time: 22:01
 */

namespace Flatmate\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\UserInterface;

/**
 * Class User
 * @package Flatmate\UserBundle\Entity
 */
class User extends BaseUser implements UserInterface{
    /**
     * @var int
     */
    protected  $id;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

    }

} 