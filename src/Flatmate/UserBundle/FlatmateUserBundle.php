<?php

namespace Flatmate\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class FlatmateUserBundle extends Bundle
{
    /**
     * Get parent
     *
     * @return string
     */
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
