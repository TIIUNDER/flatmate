<?php
/**
 * Created by PhpStorm.
 * User: cneubauer
 * Date: 12.12.14
 * Time: 11:04
 */

// src/Flatmate/UtilitiesBundle/DataFixtures/ORM/LoadCategoriesData.php

namespace Flatmate\UtilitiesBundle\DataFixture\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Flatmate\UtilitiesBundle\Entity\Category;

class LoadCategoriesData extends AbstractFixture implements OrderedFixtureInterface {

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $CategoryPower = new Category();
        $CategoryPower->setName('Strom');
        $CategoryPower->setUnit('kWH');
        $CategoryPower->setUserId('0');

        $CategoryHeat = new Category();
        $CategoryHeat->setName('Heizung');
        $CategoryHeat->setUnit('GJ');
        $CategoryHeat->setUserId('0');

        $CategoryWater = new Category();
        $CategoryWater->setName('Wasser');
        $CategoryWater->setUnit('m^3');
        $CategoryWater->setUserId('0');

        $CategoryGas = new Category();
        $CategoryGas->setName('Gas');
        $CategoryGas->setUnit('KW');
        $CategoryGas->setUserId('0');


        $manager->persist($CategoryPower);
        $manager->persist($CategoryHeat);
        $manager->persist($CategoryWater);
        $manager->persist($CategoryGas);

        $manager-> flush();

        $this ->addReference('Power-Cat',$CategoryPower);
        $this ->addReference('Heat-Cat',$CategoryHeat);
        $this ->addReference('Water-Cat',$CategoryWater);
        $this ->addReference('Gas-Cat',$CategoryGas);

    }
    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }
} 