<?php
/**
 * Created by PhpStorm.
 * User: cneubauer
 * Date: 12.12.14
 * Time: 11:16
 */

// src/Flatmate/UtilitiesBundle/DataFixtures/ORM/LoadConsumptionData.php

namespace Flatmate\UtilitiesBundle\DataFixture\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Flatmate\UtilitiesBundle\Entity\Consumption;


class LoadConsumptionData extends AbstractFixture implements OrderedFixtureInterface{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $Consuption1 = new Consumption();
        $Consuption1 ->setCategory($this->getReference('Power-Cat'));
        $Consuption1 ->setCategoryId($this->getReference('Power-Cat'));
        $Consuption1 ->setName('Stromzähler');
        $Consuption1 ->setValue('85930');
        $Consuption1 ->setUserId('0');


        $Consuption2 = new Consumption();
        $Consuption2 ->setCategory($this->getReference('Water-Cat'));
        $Consuption2 ->setCategoryId($this->getReference('Water-Cat'));
        $Consuption2 ->setName('Wasser kalt');
        $Consuption2 ->setValue('20.000');
        $Consuption2 ->setUserId('0');


        $Consuption3 = new Consumption();
        $Consuption3 ->setCategory($this->getReference('Water-Cat'));
        $Consuption3 ->setCategoryId($this->getReference('Water-Cat'));
        $Consuption3 ->setName('Wasser warm');
        $Consuption3 ->setValue('13.000');
        $Consuption3 ->setUserId('0');


        $Consuption4 = new Consumption();
        $Consuption4 ->setCategory($this->getReference('Heat-Cat'));
        $Consuption4 ->setCategoryId($this->getReference('Heat-Cat'));
        $Consuption4 ->setName('Wohnzimmer');
        $Consuption4 ->setValue('267');
        $Consuption4 ->setUserId('0');


        $Consuption5 = new Consumption();
        $Consuption5 ->setCategory($this->getReference('Heat-Cat'));
        $Consuption5 ->setCategoryId($this->getReference('Heat-Cat'));
        $Consuption5 ->setName('Bad');
        $Consuption5 ->setValue('560');
        $Consuption5 ->setUserId('0');

        $Consuption6 = new Consumption();
        $Consuption6 ->setCategory($this->getReference('Heat-Cat'));
        $Consuption6 ->setCategoryId($this->getReference('Heat-Cat'));
        $Consuption6 ->setName('Küche');
        $Consuption6 ->setValue('60');
        $Consuption6 ->setUserId('0');


        $Consuption7 = new Consumption();
        $Consuption7 ->setCategory($this->getReference('Gas-Cat'));
        $Consuption7 ->setCategoryId($this->getReference('Gas-Cat'));
        $Consuption7 ->setName('Gasherd');
        $Consuption7 ->setValue('60');
        $Consuption7 ->setUserId('0');



        $manager->persist($Consuption1);
        $manager->persist($Consuption2);
        $manager->persist($Consuption3);
        $manager->persist($Consuption4);
        $manager->persist($Consuption5);
        $manager->persist($Consuption6);
        $manager->persist($Consuption7);

        $manager->flush();



    }
    public function getOrder()
    {
        return 2; // the order in which fixtures will be loaded
    }
} 