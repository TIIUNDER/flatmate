<?php

namespace Flatmate\UtilitiesBundle\Form;

use Doctrine\ORM\EntityRepository;
use Flatmate\UtilitiesBundle\Entity\Expense;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExpenseType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $userId = $options['user_id']; // user ID out of passed user

        $categoryQuery = array(
            'class' => 'FlatmateUtilitiesBundle:Category',
            'choice_label' => 'label', // getLabel() method of Category entity
            'query_builder' => function(EntityRepository $entityRepository) use ($userId) {
                return $entityRepository->createQueryBuilder('cat')
                    ->andWhere('cat.userId='.$userId) // WHERE user_id = current user id
                    ->orderBy('cat.name', 'ASC');
            },
            'label' => 'consumption.field.category',
        );

        $periodChoices = array(
                Expense::PERIOD_TYPE_DEFAULT => '',
                Expense::PERIOD_TYPE_DAY => 'expense.period.days',
                Expense::PERIOD_TYPE_WEEK => 'expense.period.weeks',
                Expense::PERIOD_TYPE_MONTH => 'expense.period.months',
                Expense::PERIOD_TYPE_YEAR => 'expense.period.years',
        );

        $date = new \DateTime($options['date']);

        $builder
            ->add('category', 'entity', $categoryQuery)
            ->add('date', 'date', array(
                'format' => \IntlDateFormatter::MEDIUM,
                'data' => $date,
            ))
            ->add('name', null, ['label' => 'expense.field.name'])
            ->add('count', null, ['label' => 'expense.field.count'])
            ->add('consumption', null, ['label' => 'expense.field.consumption'])
            ->add('fee', 'money', array(
                'label' => 'expense.field.fee',
                'required' => false,
            ))
            ->add('deposit', 'money', ['label' => 'expense.field.deposit'])
            ->add('feePeriodCount', null, ['label' => 'expense.field.fee_period_count'])
            ->add('feePeriodType', 'choice', array(
                'choices' => $periodChoices,
                'label' => 'expense.field.fee_period_type',
            ))
            ->add('costPerUnit', 'money', array(
                'label' => 'expense.field.cost_per_unit',
                'required' => false,
            ))
            ->add('periodCount', null, ['label' => 'expense.field.period_count'])
            ->add('periodType', 'choice', array(
                'choices' => $periodChoices,
                'label' => 'expense.field.period_type',
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Flatmate\UtilitiesBundle\Entity\Expense',
            'user_id' => null,
            'date' => "now",
            'attr'=>array('novalidate'=>'novalidate'),
            'validation_groups' => function(FormInterface $form) {
                $data = $form->getData();
                $validationGroups = array('Default');
                // validate base fee fields as group
                if($data->getFee() || $data->getFeePeriodCount() || $data->getFeePeriodType() > 0) {
                    array_push($validationGroups, 'baseFeeValidation');
                }
                // validate consumption fields as group
                if($data->getCount() || $data->getConsumption()) {
                    array_push($validationGroups, 'consumptionValidation');
                }
                // validate all consumption and cost fields as group
                if(!$data->getCount() && !$data->getConsumption() && !$data->getFee() && !$data->getFeePeriodCount() &&
                    !$data->getFeePeriodType() && !$data->getCostPerUnit() && !$data->getDeposit()) {
                    array_push($validationGroups, 'feeAndConsumptionValidation');
                }
                return $validationGroups;
            }
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'flatmate_utilitiesbundle_expense';
    }
}
