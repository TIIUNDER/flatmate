<?php

namespace Flatmate\UtilitiesBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConsumptionType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $userId = $options['user_id']; // user ID out of passed user

        if($userId != 0) {
            $categoryQuery = array(
                'class' => 'FlatmateUtilitiesBundle:Category',
                'choice_label' => 'label', // getLabel() method of Category entity
                'query_builder' => function(EntityRepository $entityRepository) use ($userId) {
                    return $entityRepository->createQueryBuilder('cat')
                        ->andWhere('cat.userId='.$userId) // WHERE user_id = current user id
                        ->orderBy('cat.name', 'ASC');
                },
                'label' => 'consumption.field.category'
            );
        } else {
            $categoryQuery = array(
                'class' => 'FlatmateUtilitiesBundle:Category',
                'choice_label' => 'label', // getLabel() method of Category entity
                'query_builder' => function(EntityRepository $entityRepository) {
                    return $entityRepository->createQueryBuilder('cat')
                        ->orderBy('cat.name', 'ASC');
                },
                'label' => 'consumption.field.category'
            );
        }


        $builder
            ->add('category', 'entity', $categoryQuery)
            ->add('name', null, ['label' => 'consumption.field.name'])
            ->add('value', null, ['label' => 'consumption.field.value'])
            ->add('createdAt', null, array(
                'label' => 'consumption.field.created_at',
                'attr' => array(
                    'style' => 'display: none'
                )
            )) // TODO for some reason Symfony tries to translate the options
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Flatmate\UtilitiesBundle\Entity\Consumption',
            'user_id' => null,
            'attr'=>array('novalidate'=>'novalidate'),
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'flatmate_utilitiesbundle_consumption';
    }
}
