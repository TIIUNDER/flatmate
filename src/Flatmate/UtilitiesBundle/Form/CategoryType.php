<?php

namespace Flatmate\UtilitiesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, ['label' => 'category.field.name'])
            ->add('unit', null, ['label' => 'category.field.unit'])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Flatmate\UtilitiesBundle\Entity\Category',
            'attr'=>array('novalidate'=>'novalidate'),
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'flatmate_utilitiesbundle_category';
    }
}
