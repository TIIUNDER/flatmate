<?php

namespace Flatmate\UtilitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Expense
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Expense
{
    const PERIOD_TYPE_DEFAULT = 0;
    const PERIOD_TYPE_DAY = 1; // daily
    const PERIOD_TYPE_WEEK = 2; // weekly
    const PERIOD_TYPE_MONTH = 3; // monthly
    const PERIOD_TYPE_YEAR = 4; // yearly

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="category_id", type="integer")
     */
    private $categoryId;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $category;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Flatmate\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(message="expense.name.not_blank")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="count", type="decimal", precision=15, scale=1, nullable=true)
     * @Assert\NotBlank(message="expense.count.not_blank", groups={"consumptionValidation"})
     * @Assert\NotBlank(message="expense.fee_and_consumption", groups={"feeAndConsumptionValidation"})
     */
    private $count;

    /**
     * @var string
     *
     * @ORM\Column(name="consumption", type="decimal", scale=1, nullable=true)
     * @Assert\NotBlank(message="expense.consumption.not_blank", groups={"consumptionValidation"})
     * @Assert\NotBlank(message="expense.fee_and_consumption", groups={"feeAndConsumptionValidation"})
     */
    private $consumption;

    /**
     * @var string
     *
     * @ORM\Column(name="deposit", type="decimal", scale=2, nullable=true)
     * @Assert\NotBlank(message="expense.fee_and_consumption", groups={"feeAndConsumptionValidation"})
     */
    private $deposit;

    /**
     * @var string
     *
     * @ORM\Column(name="fee", type="decimal", scale=2, nullable=true)
     * @Assert\NotBlank(message="expense.fee.not_blank", groups={"baseFeeValidation"})
     * @Assert\NotBlank(message="expense.fee_and_consumption", groups={"feeAndConsumptionValidation"})
     */
    private $fee;

    /**
     * @var integer
     *
     * @ORM\Column(name="fee_period_count", type="integer", nullable=true)
     * @Assert\NotBlank(message="expense.fee_period_count.not_blank", groups={"baseFeeValidation"})
     * @Assert\NotBlank(message="expense.fee_and_consumption", groups={"feeAndConsumptionValidation"})
     */
    private $feePeriodCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="fee_period_type", type="smallint", nullable=true)
     * @Assert\GreaterThan(message="expense.fee_period_type.not_blank", value=0, groups={"baseFeeValidation"})
     * @Assert\GreaterThan(message="expense.fee_and_consumption", value=0, groups={"feeAndConsumptionValidation"})
     */
    private $feePeriodType;

    /**
     * @var string
     *
     * @ORM\Column(name="cost_per_unit", type="decimal", scale=4, nullable=true)
     * @Assert\NotBlank(message="expense.fee_and_consumption", groups={"feeAndConsumptionValidation"})
     */
    private $costPerUnit;

    /**
     * @var integer
     *
     * @ORM\Column(name="period_count", type="integer")
     * @Assert\NotBlank(message="expense.period_count.not_blank")
     */
    private $periodCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="period_type", type="smallint")
     * @Assert\GreaterThan(message="expense.period_type.not_blank", value=0)
     */
    private $periodType;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoryId
     *
     * @param integer $categoryId
     *
     * @return Expense
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    /**
     * Get categoryId
     *
     * @return integer
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }


    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Expense
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $date
     *
     * @return Expense
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Expense
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set count
     *
     * @param integer $count
     *
     * @return Expense
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return integer
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set consumption
     *
     * @param integer $consumption
     *
     * @return Expense
     */
    public function setConsumption($consumption)
    {
        $this->consumption = $consumption;

        return $this;
    }

    /**
     * Get consumption
     *
     * @return integer
     */
    public function getConsumption()
    {
        return $this->consumption;
    }

    /**
     * Get deposit
     *
     * @return string
     */
    public function getDeposit()
    {
        return $this->deposit;
    }

    /**
     * Set deposit
     *
     * @param string $deposit
     */
    public function setDeposit($deposit)
    {
        $this->deposit = $deposit;
    }

    /**
     * Set fee
     *
     * @param float $fee
     *
     * @return Expense
     */
    public function setFee($fee)
    {
        $this->fee = $fee;

        return $this;
    }

    /**
     * Get fee
     *
     * @return float
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * Set period
     *
     * @param integer $feePeriodCount
     *
     * @return Expense
     */
    public function setFeePeriodCount($feePeriodCount)
    {
        $this->feePeriodCount = $feePeriodCount;

        return $this;
    }

    /**
     * Get period
     *
     * @return integer
     */
    public function getFeePeriodCount()
    {
        return $this->feePeriodCount;
    }

    /**
     * @return int
     */
    public function getFeePeriodType()
    {
        return $this->feePeriodType;
    }

    /**
     * @param int $feePeriodType
     */
    public function setFeePeriodType($feePeriodType)
    {
        $this->feePeriodType = $feePeriodType;
    }

    /**
     * Set cost
     *
     * @param float $costPerUnit
     *
     * @return Expense
     */
    public function setCostPerUnit($costPerUnit)
    {
        $this->costPerUnit = $costPerUnit;

        return $this;
    }

    /**
     * Get cost
     *
     * @return string
     */
    public function getCostPerUnit()
    {
        return $this->costPerUnit;
    }

    /**
     * Get period count
     *
     * @return int
     */
    public function getPeriodCount()
    {
        return $this->periodCount;
    }

    /**
     * Set period count
     *
     * @param int $periodCount
     */
    public function setPeriodCount($periodCount)
    {
        $this->periodCount = $periodCount;
    }

    /**
     * Get period type
     *
     * @return int
     */
    public function getPeriodType()
    {
        return $this->periodType;
    }

    /**
     * Set period type
     *
     * @param int $periodType
     */
    public function setPeriodType($periodType)
    {
        $this->periodType = $periodType;
    }


}

