<?php

namespace Flatmate\UtilitiesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Category
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(message="category.name.not_blank")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="unit", type="string", length=255)
     * @Assert\NotBlank(message="category.unit.not_blank")
     */
    private $unit;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Flatmate\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set unit
     *
     * @param string $unit
     * @return Category
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit
     *
     * @return string 
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Set User ID
     *
     * @param $userId
     */
    public function setUserId($userId) {
        $this->userId = $userId;
    }

    /**
     * Get User ID
     *
     * @return int
     */
    public function getUserId() {
        return $this->userId;
    }

    /**
     * Get User
     *
     * @return string
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set User
     *
     * @param User
     */
    public function setUser($user) {
        $this->user = $user;
    }

    /**
     * To String
     *
     * @return mixed
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get Label
     *
     * @return string
     */
    public function getLabel() {
        // combine label out of name and unit
        return $this->name.' ('.$this->unit.')';
    }

}
