<?php
namespace Flatmate\UtilitiesBundle\Helper;

use Doctrine\Common\Persistence\ObjectManager;
use Flatmate\UserBundle\Entity\User;
use Flatmate\UtilitiesBundle\Entity\Consumption;
use Flatmate\UtilitiesBundle\Entity\Expense;

class ForecastHelper
{
    /**
     * Generate forecasts out of expenses and consumptions
     *
     * @param ObjectManager $em
     * @param User $user
     *
     * @return $forecasts
     */
    public static function generateForecasts($em, $user) {

        $userId = $user->getId();

        $expenses = $em->getRepository('FlatmateUtilitiesBundle:Expense')->findByUserId($userId);

        $forecasts = array();

        /** @var Expense $expense */
        foreach($expenses as $expense) {
            $expenseCategory = $expense->getCategory();

            // TODO use mutliple consumptions
            $consumption = $em->getRepository('FlatmateUtilitiesBundle:Consumption')->findOneBy(
                array(
                    'userId' => $userId,
                    'category' => $expenseCategory,
                ),
                array(
                    'createdAt' => 'DESC',
                )
            );

            $startDate = $expense->getDate();

            // period type strings for date interval
            switch($expense->getPeriodType()) {
                case Expense::PERIOD_TYPE_YEAR:
                    $periodTypeString = "years";
                    break;
                case Expense::PERIOD_TYPE_MONTH:
                    $periodTypeString = "months";
                    break;
                case Expense::PERIOD_TYPE_WEEK:
                    $periodTypeString = "weeks";
                    break;
                case Expense::PERIOD_TYPE_DAY:
                    $periodTypeString = "days";
                    break;
                default:
                    $periodTypeString = "";
                    break;
            }

            // (!) DateTime objects are not immutable and change all DateTime objects
            // for adding new time (enddate) we need an immutable DateTime (PHP >5.5)
            $newStartDate = new \DateTimeImmutable($startDate->format('Y-m-d'));
            $endDate = $newStartDate->add(
                \DateInterval::createFromDateString(
                    $expense->getPeriodCount().' '.$periodTypeString
                )
            )->format('Y-m-d');

            // TODO use entity class (without ORM)?
            array_push($forecasts,
                array(
                    'id' => $expense->getId(),
                    'startDate' => $newStartDate->format('Y-m-d'),
                    'endDate' => $endDate,
                    'consumptionUntilNow' => ForecastHelper::calcConsumptionUntilNow($expense, $consumption),
                    'expectedConsumption' => ForecastHelper::calcExpectedConsumption($expense, $consumption),
                    'expectedCosts' => ForecastHelper::calcExpectedCosts($expense, $consumption),
                    'costsUntilNow' => ForecastHelper::calcCostsUntilNow($expense, $consumption),
                    'expense' => $expense,
                    'consumption' => $consumption,
                    'category' => $expenseCategory,
                )
            );
        }

        return $forecasts;
    }

    /**
     * @param Expense $expense
     * @param Consumption $consumption
     *
     * @return mixed
     */
    public static function calcCostsUntilNow($expense, $consumption) {

        if($consumption != NULL) {
            // calculate first deposit
            $costsUntilNow = floatval($expense->getDeposit());

            // calculate cost per unit
            $costsUntilNow = $costsUntilNow + (floatval($consumption->getValue()) - $expense->getCount()) * $expense->getCostPerUnit();

            // number of items per period type
            switch($expense->getFeePeriodType()) {
                case Expense::PERIOD_TYPE_YEAR :
                    $feePeriodMultiplier = 1; // 1 year = 1 year
                    break;
                case Expense::PERIOD_TYPE_MONTH :
                    $feePeriodMultiplier = 12; // 1 year = 12 months
                    break;
                case Expense::PERIOD_TYPE_WEEK :
                    $feePeriodMultiplier = 52; // 1 year = 52 weeks
                    break;
                case Expense::PERIOD_TYPE_DAY :
                    // TODO leapyear 366 days
                    $feePeriodMultiplier = 365; // 1 year = 365 days
                    break;
                default :
                    $feePeriodMultiplier = 1;
                    break;
            }

            // number of days between expense and last consumption date
            $expenseDate = $expense->getDate();
            $consumptionDate = $consumption->getCreatedAt();
            $days = $expenseDate->diff($consumptionDate)->days+1;

            // calculate base fee amount
            $costsUntilNow = $costsUntilNow + ((($expense->getFee()*$feePeriodMultiplier)/365)/$expense->getFeePeriodCount() * $days);
        } else {
            $costsUntilNow = 0;
        }

        return round($costsUntilNow, 2);
    }

    /**
     * @param Expense $expense
     * @param Consumption $consumption
     *
     * @return int
     */
    public static function calcExpectedCosts($expense, $consumption) {

        if($consumption != NULL) {
            // calculate first deposit
            $expectedCosts = floatval($expense->getDeposit());

            // number of days between expense and last consumption date
            $expenseDate = $expense->getDate();
            $consumptionDate = $consumption->getCreatedAt();
            $days = $expenseDate->diff($consumptionDate)->days+1;

            // number of days per period day
            switch($expense->getPeriodType()) {
                case Expense::PERIOD_TYPE_YEAR :
                    // TODO leapyear 366 days
                    $periodMultiplierDays = 365; // 1 year = 365 days
                    break;
                case Expense::PERIOD_TYPE_MONTH :
                    // TODO get for current month like 28,29,30,31
                    $periodMultiplierDays = 30; // 1 month = 30 days
                    break;
                case Expense::PERIOD_TYPE_WEEK :
                    $periodMultiplierDays = 7; // 1 week = 7 days
                    break;
                case Expense::PERIOD_TYPE_DAY :
                    $periodMultiplierDays = 1; // 1 day = 1 day
                    break;
                default :
                    $periodMultiplierDays = 0;
                    break;
            }

            // calculate cost per unit and extrapolate to end date of expense
            $expectedCosts = $expectedCosts + (((floatval($consumption->getValue()) - $expense->getCount()) * floatval($expense->getCostPerUnit()))/$days) * $periodMultiplierDays * $expense->getPeriodCount();

            // number of items per period type
            switch($expense->getFeePeriodType()) {
                case Expense::PERIOD_TYPE_YEAR :
                    $feePeriodMultiplier = 1; // 1 year = 1 year
                    break;
                case Expense::PERIOD_TYPE_MONTH :
                    $feePeriodMultiplier = 12; // 1 year = 12 months
                    break;
                case Expense::PERIOD_TYPE_WEEK :
                    $feePeriodMultiplier = 52; // 1 year = 52 weeks
                    break;
                case Expense::PERIOD_TYPE_DAY :
                    // TODO leapyear 366 days
                    $feePeriodMultiplier = 365; // 1 year = 365 days
                    break;
                default:
                    $feePeriodMultiplier = 0;
                    break;
            }

            // calculate base fee amount
            $expectedCosts = $expectedCosts + (($expense->getFee() * $feePeriodMultiplier)/$expense->getFeePeriodCount());

        } else {
            $expectedCosts = 0;
        }

        return $expectedCosts;
    }

    /**
     * @param Expense $expense
     * @param Consumption $consumption
     *
     * @return int
     */
    public static function calcConsumptionUntilNow($expense, $consumption) {

        if($consumption != NULL && $expense != NULL) {
            $consumptionUntilNow = $consumption->getValue() - $expense->getCount();
        } else {
            $consumptionUntilNow = 0;
        }

        return $consumptionUntilNow;

    }

    /**
     * @param Expense $expense
     * @param Consumption $consumption
     *
     * @return float|int
     */
    public static function calcExpectedConsumption($expense, $consumption) {

        if($consumption != NULL && $expense != NULL) {
            // calculate consumption until now
            $expectedConsumption = self::calcConsumptionUntilNow($expense, $consumption);

            // number of days between expense and last consumption date
            $expenseDate = $expense->getDate();
            $consumptionDate = $consumption->getCreatedAt();
            $days = $expenseDate->diff($consumptionDate)->days+1;

            // number of days per period day
            switch($expense->getPeriodType()) {
                case Expense::PERIOD_TYPE_YEAR :
                    // TODO leapyear 366 days
                    $periodMultiplierDays = 365; // 1 year = 365 days
                    break;
                case Expense::PERIOD_TYPE_MONTH :
                    // TODO get for current month like 28,29,30,31
                    $periodMultiplierDays = 30; // 1 month = 30 days
                    break;
                case Expense::PERIOD_TYPE_WEEK :
                    $periodMultiplierDays = 7; // 1 week = 7 days
                    break;
                case Expense::PERIOD_TYPE_DAY :
                    $periodMultiplierDays = 1; // 1 day = 1 day
                    break;
                default :
                    $periodMultiplierDays = 0;
                    break;
            }

            // extrapolate to expected costs
            $expectedConsumption = ($expectedConsumption/$days) * ($periodMultiplierDays * $expense->getPeriodCount());
        } else {
            $expectedConsumption = 0;
        }

        return $expectedConsumption;
    }
}