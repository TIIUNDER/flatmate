<?php

namespace Flatmate\UtilitiesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Flatmate\UtilitiesBundle\Entity\Consumption;
use Flatmate\UtilitiesBundle\Form\ConsumptionType;

/**
 * Consumption controller.
 *
 */
class ConsumptionController extends Controller
{

    /**
     * Lists all Consumption entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $userId = $this->getUser()->getId();

        $entities = $em->getRepository('FlatmateUtilitiesBundle:Consumption')->findByUserId($userId, array('createdAt' => 'DESC'));

        return $this->render('FlatmateUtilitiesBundle:Consumption:index.html.twig', array(
            'entities' => $entities
        ));
    }
    /**
     * Creates a new Consumption entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Consumption();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $entity->setUser($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('consumption_show', array('id' => $entity->getId())));
        }

        return $this->render('FlatmateUtilitiesBundle:Consumption:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'category' => $this->hasCategories(),
        ));
    }

    /**
     * Creates a form to create a Consumption entity.
     *
     * @param Consumption $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Consumption $entity)
    {
        $form = $this->createForm(new ConsumptionType(), $entity, array(
            'action' => $this->generateUrl('consumption_create'),
            'method' => 'POST',
            'user_id' => $this->getUser()->getId(), // pass user to form
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Consumption entity.
     *
     */
    public function newAction()
    {
        $entity = new Consumption();
        $form   = $this->createCreateForm($entity);

        return $this->render('FlatmateUtilitiesBundle:Consumption:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'category' => $this->hasCategories(),
        ));
    }

    /**
     * Finds and displays a Consumption entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FlatmateUtilitiesBundle:Consumption')->find($id);

        $userId = $this->getUser()->getId();

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.entity_not_found'));
        }

        // check for correct user
        if($userId != $entity->getUserId()) {
            throw $this->createAccessDeniedException($this->get('translator')->trans('exception.access_denied'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FlatmateUtilitiesBundle:Consumption:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Consumption entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FlatmateUtilitiesBundle:Consumption')->find($id);

        $userId = $this->getUser()->getId();

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.entity_not_found'));
        }

        // check for correct user
        if($userId != $entity->getUserId()) {
            throw $this->createAccessDeniedException($this->get('translator')->trans('exception.access_denied'));
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FlatmateUtilitiesBundle:Consumption:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Consumption entity.
    *
    * @param Consumption $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Consumption $entity)
    {
        $form = $this->createForm(new ConsumptionType(), $entity, array(
            'action' => $this->generateUrl('consumption_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'user_id' => $this->getUser()->getId(), // pass user to form
        ));

        $form->add('submit', 'submit', array('label' => 'consumption.update'));

        return $form;
    }
    /**
     * Edits an existing Consumption entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FlatmateUtilitiesBundle:Consumption')->find($id);

        $userId = $this->getUser()->getId();

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.entity_not_found'));
        }

        // check for correct user
        if($userId != $entity->getUserId()) {
            throw $this->createAccessDeniedException($this->get('translator')->trans('exception.access_denied'));
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('consumption_show', array('id' => $id)));
        }

        return $this->render('FlatmateUtilitiesBundle:Consumption:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Consumption entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        $userId = $this->getUser()->getId();

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('FlatmateUtilitiesBundle:Consumption')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException($this->get('translator')->trans('exception.entity_not_found'));
            }

            // check for correct user
            if($userId != $entity->getUserId()) {
                throw $this->createAccessDeniedException($this->get('translator')->trans('exception.access_denied'));
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('consumption'));
    }

    /**
     * Creates a form to delete a Consumption entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('consumption_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'label' => 'consumption.delete',
                'attr' => array(
                    'class' => 'btn-primary',
                    'onclick' => 'return confirm("'.$this->get('translator')->trans('consumption.delete_confirm').'")'
                )
            ))
            ->getForm()
        ;
    }

    /**
     * Checks if there are categories
     *
     * @return bool
     */
    private function hasCategories() {
        $userId = $this->getUser()->getId();

        // get category repository
        $categoryRepo = $this->getDoctrine()->getManager()->getRepository('FlatmateUtilitiesBundle:Category');

        // search for existing categories for user
        $categoryExistsByUser = $categoryRepo->findByUserId($userId);

        // check if user or public user has already categories and mark as true or false
        if($categoryExistsByUser && $userId != 0) {
            $category = true;
        } else  {
            $category = false;
        }

        return $category;
    }
}
