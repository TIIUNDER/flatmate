<?php
namespace Flatmate\UtilitiesBundle\Controller;

use Flatmate\UtilitiesBundle\Entity\Expense;
use Flatmate\UtilitiesBundle\Entity\Consumption;
use Flatmate\UtilitiesBundle\Helper\ForecastHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ForecastController extends Controller
{
    /**
     * Index action
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction() {

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $forecasts = ForecastHelper::generateForecasts($em, $user);

        return $this->render('FlatmateUtilitiesBundle:Forecast:index.html.twig', array(
            'forecasts' => $forecasts,
        ));
    }

    /**
     * Show action
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id) {

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $forecasts = ForecastHelper::generateForecasts($em, $user);

        $entity = null;
        foreach($forecasts as $forecast) {
            if($forecast['id'] == $id) {
                $entity = $forecast;
                break;
            }
        }

        return $this->render('FlatmateUtilitiesBundle:Forecast:show.html.twig', array(
            'entity'      => $entity,
        ));
    }
}