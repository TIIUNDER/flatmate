<?php

namespace Flatmate\UtilitiesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Flatmate\UtilitiesBundle\Entity\Expense;
use Flatmate\UtilitiesBundle\Form\ExpenseType;

/**
 * Expense controller.
 *
 */
class ExpenseController extends Controller
{

    /**
     * Lists all Expense entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $userId = $this->getUser()->getId();

        $entities = $em->getRepository('FlatmateUtilitiesBundle:Expense')->findByUserId($userId, array('date' => 'DESC'));

        return $this->render('FlatmateUtilitiesBundle:Expense:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Expense entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Expense();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $entity->setUser($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('expense_show', array('id' => $entity->getId())));
        }

        return $this->render('FlatmateUtilitiesBundle:Expense:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'category' => $this->hasCategories(),
        ));
    }

    /**
     * Creates a form to create a Expense entity.
     *
     * @param Expense $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Expense $entity)
    {
        $form = $this->createForm(new ExpenseType(), $entity, array(
            'action' => $this->generateUrl('expense_create'),
            'method' => 'POST',
            'user_id' => $this->getUser()->getId(), // pass user to form
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Expense entity.
     *
     */
    public function newAction()
    {
        $entity = new Expense();
        $form   = $this->createCreateForm($entity);

        return $this->render('FlatmateUtilitiesBundle:Expense:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'category' => $this->hasCategories(),
        ));
    }

    /**
     * Finds and displays a Expense entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FlatmateUtilitiesBundle:Expense')->find($id);

        $userId = $this->getUser()->getId();

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.entity_not_found'));
        }

        // check for correct user
        if($userId != $entity->getUserId()) {
            throw $this->createAccessDeniedException($this->get('translator')->trans('exception.access_denied'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FlatmateUtilitiesBundle:Expense:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Expense entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FlatmateUtilitiesBundle:Expense')->find($id);

        $userId = $this->getUser()->getId();

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.entity_not_found'));
        }

        // check for correct user
        if($userId != $entity->getUserId()) {
            throw $this->createAccessDeniedException($this->get('translator')->trans('exception.access_denied'));
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FlatmateUtilitiesBundle:Expense:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Expense entity.
    *
    * @param Expense $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Expense $entity)
    {
        $form = $this->createForm(new ExpenseType(), $entity, array(
            'action' => $this->generateUrl('expense_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'user_id' => $this->getUser()->getId(), // pass user to form
            'date' => $entity->getDate()->format('Y-m-d H:i:s'),
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Expense entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FlatmateUtilitiesBundle:Expense')->find($id);

        $userId = $this->getUser()->getId();

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.entity_not_found'));
        }

        // check for correct user
        if($userId != $entity->getUserId()) {
            throw $this->createAccessDeniedException($this->get('translator')->trans('exception.access_denied'));
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('expense_show', array('id' => $id)));
        }

        return $this->render('FlatmateUtilitiesBundle:Expense:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Expense entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        $userId = $this->getUser()->getId();

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('FlatmateUtilitiesBundle:Expense')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException($this->get('translator')->trans('exception.entity_not_found'));
            }

            // check for correct user
            if($userId != $entity->getUserId()) {
                throw $this->createAccessDeniedException($this->get('translator')->trans('exception.access_denied'));
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('expense'));
    }

    /**
     * Creates a form to delete a Expense entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('expense_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'label' => 'expense.delete',
                'attr' => array(
                    'class' => 'btn-primary',
                    'onclick' => 'return confirm("'.$this->get('translator')->trans('expense.delete_confirm').'")'
                )
            ))
            ->getForm();
    }

    /**
     * Checks if there are categories
     *
     * @return bool
     */
    private function hasCategories() {
        $userId = $this->getUser()->getId();

        // get category repository
        $categoryRepo = $this->getDoctrine()->getManager()->getRepository('FlatmateUtilitiesBundle:Category');

        // search for existing categories for user
        $categoryExistsByUser = $categoryRepo->findByUserId($userId);

        // check if user or public user has already categories and mark as true or false
        if($categoryExistsByUser && $userId != 0) {
            $category = true;
        } else  {
            $category = false;
        }

        return $category;
    }
}
