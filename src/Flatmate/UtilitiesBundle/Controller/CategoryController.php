<?php

namespace Flatmate\UtilitiesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Flatmate\UtilitiesBundle\Entity\Category;
use Flatmate\UtilitiesBundle\Form\CategoryType;

/**
 * Category controller.
 *
 */
class CategoryController extends Controller
{

    /**
     * Lists all Category entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $userId = $this->getUser()->getId();

        // find own data of user
        $entities = $em->getRepository('FlatmateUtilitiesBundle:Category')->findByUserId($userId);

        return $this->render('FlatmateUtilitiesBundle:Category:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Category entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Category();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $entity->setUser($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('category_show', array('id' => $entity->getId())));
        }

        return $this->render('FlatmateUtilitiesBundle:Category:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Category entity.
     *
     * @param Category $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Category $entity)
    {
        $form = $this->createForm(new CategoryType(), $entity, array(
            'action' => $this->generateUrl('category_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'category.create',
            'attr' => array(
                'class' => 'btn-primary'
            )
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Category entity.
     *
     */
    public function newAction()
    {
        $entity = new Category();
        $form   = $this->createCreateForm($entity);

        return $this->render('FlatmateUtilitiesBundle:Category:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Category entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FlatmateUtilitiesBundle:Category')->find($id);

        $userId = $this->getUser()->getId();

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.entity_not_found'));
        }

        // check for correct user
        if($userId != $entity->getUserId()) {
            throw $this->createAccessDeniedException($this->get('translator')->trans('exception.access_denied'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FlatmateUtilitiesBundle:Category:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Category entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FlatmateUtilitiesBundle:Category')->find($id);

        $userId = $this->getUser()->getId();

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.entity_not_found'));
        }

        // check for correct user
        if($userId != $entity->getUserId()) {
            throw $this->createAccessDeniedException($this->get('translator')->trans('exception.access_denied'));
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FlatmateUtilitiesBundle:Category:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Category entity.
    *
    * @param Category $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Category $entity)
    {
        $form = $this->createForm(new CategoryType(), $entity, array(
            'action' => $this->generateUrl('category_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->
            add('submit', 'submit', array(
            'label' => 'category.update',
            'attr' => array(
                'class' => 'btn-primary'
            )
        ));

        return $form;
    }
    /**
     * Edits an existing Category entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FlatmateUtilitiesBundle:Category')->find($id);

        $userId = $this->getUser()->getId();

        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('exception.entity_not_found'));
        }

        // check for correct user
        if($userId != $entity->getUserId()) {
            throw $this->createAccessDeniedException($this->get('translator')->trans('exception.access_denied'));
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('category_show', array('id' => $id)));
        }

        return $this->render('FlatmateUtilitiesBundle:Category:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Category entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        $userId = $this->getUser()->getId();

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('FlatmateUtilitiesBundle:Category')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException($this->get('translator')->trans('exception.entity_not_found'));
            }

            // check for correct user
            if($userId != $entity->getUserId()) {
                throw $this->createAccessDeniedException($this->get('translator')->trans('exception.access_denied'));
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('category'));
    }

    /**
     * Creates a form to delete a Category entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('category_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'label' => 'category.delete',
                'attr' => array(
                    'class' => 'btn-primary',
                    'onclick' => 'return confirm("'.$this->get('translator')->trans('category.delete_confirm').'")'

                )
            ))
            ->getForm()
        ;
    }
}
