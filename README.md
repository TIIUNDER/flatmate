[![Build Status](https://travis-ci.org/TIIUNDER/flatmate.svg?branch=master)](https://travis-ci.org/TIIUNDER/flatmate)

# Flatmate

Symfony Web App

## Documentation

* [Setup](https://github.com/TIIUNDER/flatmate/wiki/Setup)
* [Webserver](https://github.com/TIIUNDER/flatmate/wiki/Webserver)
* [REST API](https://github.com/TIIUNDER/flatmate/wiki/REST-API)
* [User Roles](https://github.com/TIIUNDER/flatmate/wiki/User-Roles)
* [Links](https://github.com/TIIUNDER/flatmate/wiki/Links)

See [Wiki](https://github.com/TIIUNDER/flatmate/wiki) for Documentation and more information

## Links

* [Android-App](https://github.com/TIIUNDER/flatmate-android/)

## License

[MIT License](http://opensource.org/licenses/MIT)

See [LICENSE](https://github.com/TIIUNDER/flatmate/blob/master/LICENSE) for more information
